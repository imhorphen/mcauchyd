You can use the [multvardiv](https://forgemia.inra.fr/imhorphen/multvardiv) package,
which provides the same tools for the multivariate Cauchy distribution
and other multvariate distributions.

# mcauchyd

This package provides tools for multivariate Cauchy distributions:

* Calculation of distances/divergences between multivariate Cauchy distributions:
    + Kullback-Leibler divergence
* Manipulation of multivariate Cauchy distributions:
    + Probability density
    + Simulation from a MCD
    + Plot of the density of a MCD with 2 variables

## Installation

Install the package from CRAN:
```
install.package("mcauchyd")
```

Or the development version from the repository, using the [`devtools`](https://CRAN.R-project.org/package=devtools) package:
```
install.packages("devtools")
devtools::install_git("https://forgemia.inra.fr/imhorphen/mcauchyd")
```

## Authors

[Pierre Santagostini](pierre.santagostini@agrocampus-ouest.fr) and [Nizar Bouhlel](nizar.bouhlel@agrocampus-ouest.fr)

## Reference

N. Bouhlel, D. Rousseau,
A Generic Formula and Some Special Cases for the Kullback–Leibler Divergence between Central Multivariate Cauchy Distributions.
Entropy, 24, 838, July 2022.
[https://doi.org/10.3390/e24060838](https://doi.org/10.3390/e24060838)
